class SortSumScoreFunc {
  names!: string[];
  constructor() {
    try {
      let data = fs.readFileSync('names.txt', 'utf8');
      this.names = data.replace(/"/g, '').split(',');
    } catch (e) {
      this.names = [];
    }
  }
  sortNames() {
    this.names.sort();
  }
  sumName(name: string) {
    let sum = 0;
    name.split('').forEach(letter => sum += (letter.toUpperCase()).charCodeAt(0) - 64);
    return sum;
  }
  getScore() {
    let score = 0;
    this.sortNames()
    this.names.forEach((name, index) => { score += this.sumName(name) * (index + 1) });
    return score;
  }
}

var fs = require('fs');
console.log(new SortSumScoreFunc().getScore());