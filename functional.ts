import * as fs from 'fs';

function sumName(name: string) {
  let sum = 0;
  name.split('').forEach(letter => sum += (letter.toUpperCase()).charCodeAt(0) - 64);
  return sum;
}

function getNames() {
  try {
    let data = fs.readFileSync('names.txt', 'utf8');
    return data.replace(/"/g, '').split(',');

  } catch (e) {
    return [];
  }
}

let sums: number[] = getNames().sort().map(sumName);
console.log(sums.map((sum, index) => sum * (index + 1)).reduce((total, current) => total + current, 0));
