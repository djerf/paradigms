var fs = require('fs');

let namesInput: string[];

const sortSumScore = (names: string[]) => {
  let score = 0;
  // sort names
  let sortedNames = names.sort();
  sortedNames.forEach((name, index) => {
    let nameSum = 0;
    // sum each name
    name.split('').forEach(letter => nameSum += (letter.toUpperCase()).charCodeAt(0) - 64);
    // update total score
    score += nameSum * (index + 1);
  });
  return score;
}

try {
  var data = fs.readFileSync('names.txt', 'utf8');
  // remove double quotes, format as array
  namesInput = data.replace(/"/g, '').split(',');
  console.log(sortSumScore(namesInput));
} catch (e) {
  console.log('Error:', e.stack);
}